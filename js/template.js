(function($) { "use strict";

	//Navigation

	$('ul.slimmenu').on('click',function(){
		var width = $(window).width();
		if ((width <= 800)){
			$(this).slideToggle();}
	});

	$('ul.slimmenu').slimmenu(
	{
		resizeWidth: '800',
		collapserTitle: '',
		easingEffect:'easeInOutQuint',
		animSpeed:'medium',
		indentChildren: true,
		childrenIndenter: '&raquo;'
	});


	//Parallax

	$(document).ready(function(){
			$('.parallax-home').parallax("50%", 0.4);
			$('.parallax-1').parallax("50%", 0.4);
			$('.parallax-2').parallax("50%", 0.4);
			$('.parallax-3').parallax("50%", 0.4);
			$('.parallax-4').parallax("50%", 0.4);
			$('.parallax-5').parallax("50%", 0.4);
	});



  })(jQuery);












 
